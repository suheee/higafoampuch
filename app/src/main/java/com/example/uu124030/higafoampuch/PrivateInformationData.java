package com.example.uu124030.higafoampuch;

import android.os.Parcel;
import android.os.Parcelable;

public class PrivateInformationData implements Parcelable {
    private String confirmationName;
    private String confirmationSex;
    private String confirmationTell;
    private String confirmationHobby;
    private String confirmationJob;

    PrivateInformationData() {}

    private PrivateInformationData(Parcel in) {
        confirmationName = in.readString();
        confirmationSex = in.readString();
        confirmationTell = in.readString();
        confirmationHobby = in.readString();
        confirmationJob = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(confirmationName);
        dest.writeString(confirmationSex);
        dest.writeString(confirmationTell);
        dest.writeString(confirmationHobby);
        dest.writeString(confirmationJob);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PrivateInformationData> CREATOR = new Creator<PrivateInformationData>() {
        @Override
        public PrivateInformationData createFromParcel(Parcel in) {
            return new PrivateInformationData(in);
        }

        @Override
        public PrivateInformationData[] newArray(int size) {
            return new PrivateInformationData[size];
        }
    };


    // getter
    public String getConfirmationName() {
        return confirmationName;
    }

    public String getConfirmationSex() {
        return confirmationSex;
    }

    public String getConfirmationTell() {
        return confirmationTell;
    }

    public String getConfirmationHobby() {
        return confirmationHobby;
    }

    public String getConfirmationJob() {
        return confirmationJob;
    }

    // setter

    public void setConfirmationName(String confirmationName) {
        this.confirmationName = confirmationName;
    }

    public void setConfirmationSex(String confirmationSex) {
        this.confirmationSex = confirmationSex;
    }

    public void setConfirmationTell(String confirmationTell) {
        this.confirmationTell = confirmationTell;
    }

    public void setConfirmationHobby(String confirmationHobby) {
        this.confirmationHobby = confirmationHobby;
    }

    public void setConfirmationJob(String confirmationJob) {
        this.confirmationJob = confirmationJob;
    }
}
