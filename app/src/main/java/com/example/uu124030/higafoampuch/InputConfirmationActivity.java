package com.example.uu124030.higafoampuch;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class InputConfirmationActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView confirmationName;
    TextView confirmationSex;
    TextView confirmationTell;
    TextView confirmationHobby;
    TextView confirmationJob;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_confirmation_activity);
        toolbar = (Toolbar) findViewById(R.id.confirmation_toolbar);
        toolbar.setTitle("入力内容の確認");
        toolbar.setNavigationIcon(R.mipmap.left_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        confirmationName = (findViewById(R.id.confirmation_name));
        confirmationSex = (findViewById(R.id.confirmation_sex));
        confirmationTell = (findViewById(R.id.confirmation_tell));
        confirmationHobby = (findViewById(R.id.confirmation_hobby));
        confirmationJob = (findViewById(R.id.confirmation_job));

        Intent intent = getIntent();
        PrivateInformationData informationData = intent.getParcelableExtra(InputFoamActivity.INPUT_CONFIRMATION_EXTRA);
        confirmationName.setText(informationData.getConfirmationName());
        confirmationSex.setText(informationData.getConfirmationSex());
        confirmationTell.setText(informationData.getConfirmationTell());
        confirmationHobby.setText(informationData.getConfirmationHobby());
        confirmationJob.setText(informationData.getConfirmationJob());
    }
}
