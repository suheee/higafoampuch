package com.example.uu124030.higafoampuch;

import android.os.Parcel;
import android.os.Parcelable;

public class Hobby implements Parcelable {
    private String name;
    private boolean isChecked;

    Hobby(String name) {
        this.name = name;
        this.isChecked = false;
    }

    protected Hobby(Parcel in) {
        name = in.readString();
        isChecked = in.readByte() != 0;
    }

    public static final Creator<Hobby> CREATOR = new Creator<Hobby>() {
        @Override
        public Hobby createFromParcel(Parcel in) {
            return new Hobby(in);
        }

        @Override
        public Hobby[] newArray(int size) {
            return new Hobby[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeByte((byte) (isChecked ? 1 : 0));
    }

    public String getName() {
        return name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}