package com.example.uu124030.higafoampuch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import java.util.ArrayList;

public class HobbiesAdapter extends ArrayAdapter<Hobby> {
    private LayoutInflater inflater;

    public HobbiesAdapter(Context context, ArrayList<Hobby> resource) {
        super(context, 0, resource);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        CheckBox checkBoxViewHolder;
        if (view == null) {
            view = inflater.inflate(R.layout.hobby_checklist_item, null);
            checkBoxViewHolder = (CheckBox) view.findViewById(R.id.hobby_check_box);
            view.setTag(checkBoxViewHolder);
        } else {
            checkBoxViewHolder = (CheckBox) view.getTag();
        }
        Hobby hobby = getItem(position);
        checkBoxViewHolder.setText(hobby.getName());
        checkBoxViewHolder.setChecked(hobby.isChecked());

        return view;
    }
}
