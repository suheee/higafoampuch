package com.example.uu124030.higafoampuch;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class InputFoamActivity extends AppCompatActivity {
    private final int HOBY_ACTIVITY_REQUEST_CODE = 0;
    public static final String HOBBY_LIST_EXTRA_FROM_INPUT_FOAM_ACTIVITY
            = "HOBBY_LIST_EXTRA_FROM_INPUT_FOAM_ACTIVITY";
    public static final String INPUT_CONFIRMATION_EXTRA = "INPUT_CONFIRMATION_EXTRA";

    // View
    Toolbar toolbar;
    EditText familyNameEditText;
    EditText firstNameEditText;
    RadioGroup sexRadioGroup;
    EditText tellEditText;
    TextView hobbyTextView;
    Spinner spinner;
    TextView termsOfUseText;

    StringBuilder hobbysStringBuilder;
    ArrayList<Hobby> hobbiesList;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_foam);

        hobbysStringBuilder = new StringBuilder();

        sexRadioGroup = (RadioGroup) findViewById(R.id.SexRadioGroup);
        tellEditText = (EditText) findViewById(R.id.tell);
        spinner = (Spinner) findViewById(R.id.spinner);

        // Toolbar
        toolbar = (Toolbar) findViewById(R.id.input_foam_toolbar);
        toolbar.setTitle("入力フォーム");

        // Name
        familyNameEditText = (EditText) findViewById(R.id.family_name);
        firstNameEditText = (EditText) findViewById(R.id.first_name);

        // HobbyChangeButton
        Button hobbyChangeButton = (Button) findViewById(R.id.hobby_change_button);
        hobbyChangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hobbiesList == null) {
                    hobbiesList = new ArrayList<>();
                    for (int i = 1; i <= 20; i++) {
                        Hobby hobby = new Hobby("趣味" + i);
                        hobbiesList.add(hobby);
                    }
                }
                Intent intent = new Intent(InputFoamActivity.this, HobbyActivity.class);
                intent.putParcelableArrayListExtra(HOBBY_LIST_EXTRA_FROM_INPUT_FOAM_ACTIVITY, hobbiesList);
                startActivityForResult(intent, HOBY_ACTIVITY_REQUEST_CODE);
            }
        });

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item,
                getJobs()
        );
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);

        final Button completeButton = (Button) findViewById(R.id.complete_Button);
        completeButton.setEnabled(false);
        completeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InputFoamActivity.this, InputConfirmationActivity.class);
                if (checkNameEditText()) {
                    intent.putExtra(INPUT_CONFIRMATION_EXTRA, getPrivateInformationData());
                    startActivity(intent);
                } else {
                    showAlertDialog();
                }
            }
        });

        CheckBox checkBox = (CheckBox) findViewById(R.id.agree_check_box);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                completeButton.setEnabled(b);
            }
        });

        termsOfUseText = (TextView) findViewById(R.id.terms_of_use_text);
        termsOfUseText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InputFoamActivity.this, TermsOfUseActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == HOBY_ACTIVITY_REQUEST_CODE) {
                hobbiesList = data.getParcelableArrayListExtra(HobbyActivity.HOBBY_LIST_EXTRA_FROM_HOBBY_ACTIVITY);
                updateHobbies();
                return;
            }
        }
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("未入力の必須項目があります");
        builder.setMessage("氏名が入力されていません。");
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void updateHobbies() {
        Hobby trueHobby = null;
        ArrayList<Hobby> trueHobbyList = new ArrayList<>();
        hobbysStringBuilder = new StringBuilder();

        for (Hobby hobby : hobbiesList) {
            if (hobby.isChecked()) {
                trueHobby = hobby;
                trueHobbyList.add(trueHobby);
            }
        }
        for (int i = 0; i < trueHobbyList.size(); i++) {
            if (i != (trueHobbyList.size()) - 1) {
                hobbysStringBuilder.append(trueHobbyList.get(i).getName() + ", ");
            } else {
                hobbysStringBuilder.append(trueHobbyList.get(i).getName());
            }
        }
        hobbyTextView = (TextView) findViewById(R.id.hobby_text);
        hobbyTextView.setText(hobbysStringBuilder);
    }

    private boolean checkNameEditText() {
        String familyName = familyNameEditText.getText().toString();
        String firstName = firstNameEditText.getText().toString();
        if (TextUtils.isEmpty(familyName) || TextUtils.isEmpty(firstName)) {
            return false;
        }
        return true;
    }

    private PrivateInformationData getPrivateInformationData() {
        String noContent = getString(R.string.no_content);

        PrivateInformationData privateInformationData = new PrivateInformationData();

        String familyName = familyNameEditText.getText().toString();
        String firstName = firstNameEditText.getText().toString();
        if (TextUtils.isEmpty(familyName) || TextUtils.isEmpty(firstName)) {
            showAlertDialog();
            finish();
        }
        String name = familyName + "  " + firstName;
        privateInformationData.setConfirmationName(name);

        int id = sexRadioGroup.getCheckedRadioButtonId();
        if (id == -1) {
            privateInformationData.setConfirmationSex(noContent);
        } else {
            String sex = ((RadioButton) findViewById(id)).getText().toString();
            privateInformationData.setConfirmationSex(sex);
        }

        if (TextUtils.isEmpty(tellEditText.getText().toString())) {
            privateInformationData.setConfirmationTell(noContent);
        } else {
            String tell = tellEditText.getText().toString();
            privateInformationData.setConfirmationTell(tell);
        }

        String hobby = hobbysStringBuilder.toString();
        if (TextUtils.isEmpty(hobby)) {
            privateInformationData.setConfirmationHobby(noContent);
        } else {
            privateInformationData.setConfirmationHobby(hobby);
        }

        String job = (String) spinner.getSelectedItem();
        privateInformationData.setConfirmationJob(job);

        return privateInformationData;
    }

    private List<String> getJobs() {
        List<String> jobs = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            jobs.add("職業" + i);
        }
        return jobs;
    }
}