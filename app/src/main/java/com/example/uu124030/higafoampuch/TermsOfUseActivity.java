package com.example.uu124030.higafoampuch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class TermsOfUseActivity extends AppCompatActivity {
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_of_use);

        webView = (WebView) findViewById(R.id.web_view);
        webView.loadUrl("https://zexy-enmusubi.net");
    }
}
