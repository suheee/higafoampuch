package com.example.uu124030.higafoampuch;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class HobbyActivity extends AppCompatActivity {
    HobbiesAdapter hobbiesAdapter;
    ListView listView;
    public static final String HOBBY_LIST_EXTRA_FROM_HOBBY_ACTIVITY = "HOBBY_LIST_EXTRA_FROM_HOBBY_ACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hobby);

        addToolbar();

        listView = (ListView) findViewById(R.id.hobby_check_box_list);
        hobbiesAdapter = new HobbiesAdapter(this, getHobbyList());
        listView.setAdapter(hobbiesAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Hobby hobby = hobbiesAdapter.getItem(position);
                hobby.setChecked(!hobby.isChecked());
                hobbiesAdapter.notifyDataSetChanged();
            }
        });

        Button completeSettingButton = (Button) findViewById(R.id.completeSettingButton);
        completeSettingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                ArrayList<Hobby> hobbyList = new ArrayList<>();
                for (int i = 0; i < hobbiesAdapter.getCount(); i++) {
                    Hobby hobby = hobbiesAdapter.getItem(i);
                    hobbyList.add(hobby);
                }
                intent.putParcelableArrayListExtra(HOBBY_LIST_EXTRA_FROM_HOBBY_ACTIVITY, hobbyList);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

    }

    private void addToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.hobby_toolbar);
        toolbar.setTitle("趣味");
        toolbar.setNavigationIcon(R.mipmap.left_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toolbar.inflateMenu(R.menu.hobby_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int itemId = item.getItemId();
                if (itemId == R.id.hobby_menu) {
                    for (int i = 0; i < hobbiesAdapter.getCount(); i++) {
                        Hobby hobby = hobbiesAdapter.getItem(i);
                        hobby.setChecked(false);
                        hobbiesAdapter.notifyDataSetChanged();
                    }
                    return false;
                }
                return false;
            }
        });
    }

    private ArrayList<Hobby> getHobbyList() {
        Intent intent = getIntent();
        ArrayList<Hobby> hobbiesList
                = intent.getParcelableArrayListExtra(InputFoamActivity.HOBBY_LIST_EXTRA_FROM_INPUT_FOAM_ACTIVITY);
        return hobbiesList;
    }
}
